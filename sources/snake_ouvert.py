# Créé par QUELIN, le 16/02/2024 en Python 3.7

import pygame
import random
# Initialisation de Pygame
pygame.init()


# Variables
score = 0


# Dimensions de l'écran
largeur_ecran = 1000
hauteur_ecran = 800

# Création de la fenêtre du jeu
screen = pygame.display.set_mode((largeur_ecran, hauteur_ecran))
police = pygame.font.SysFont("Arial", 36)
pygame.display.set_caption("Affichage de texte dans pygame")

# Variables pour la position du serpent
x, y = 500, 700

# Paramètres de l'obstacle
taille_obstacle = 50
vitesse_obstacle = 2

# Paramètres du serpent
taille_serpent = 20
vitesse_serpent = 15
deplacement = True

# Position initiale de l'obstacle
x_obstacle = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
y_obstacle = 0

x_obstacle1 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
y_obstacle1 = 0

x_obstacle2 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
y_obstacle2 = 0

x_obstacle3 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
y_obstacle3 = 0

x_obstacle4 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
y_obstacle4 = 0

x_obstacle5 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
y_obstacle5 = 0

x_obstacle6 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
y_obstacle6 = 0

x_obstacle7 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
y_obstacle7 = 0

x_obstacle8 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
y_obstacle8 = 0






# Position initiale de la nourriture
x_nourriture = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
y_nourriture = y_nourriture2 = y_nourriture3 = y_nourriture4 = y_bonus = 0
x_nourriture2 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
x_nourriture3 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
x_nourriture4 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)

# Couleurs
orange = (255,184,60)
noir = (0, 0, 0)
rouge = (255, 0, 0)
gris = (100, 100, 100)
blanc = (255, 255, 255)
# Boucle principale du jeu
done = False
is_blue = True
clock = pygame.time.Clock()

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
            is_blue = not is_blue

    # Déplacement du serpent
    pressed = pygame.key.get_pressed()

    if pressed[pygame.K_LEFT]:
        x -= 8
    if pressed[pygame.K_RIGHT]:
        x += 8
    if deplacement == True:
        y_nourriture += 4
        y_nourriture2 += 2
        y_nourriture3 += 5
        y_nourriture4 += 3
        y_bonus += 5
    if deplacement == True:
        y_obstacle += 5
        y_obstacle1 += 5
        y_obstacle2 += 5
        y_obstacle3 += 5
        y_obstacle4 += 5
        y_obstacle5 += 5
        y_obstacle6 += 5
        y_obstacle7 += 5
        y_obstacle8 += 5
    if x <=-150 :
        x = 990
    if x >= 1000 :
        x = -140
    # Affichage du serpent
    screen.fill(orange)


    serpent = pygame.image.load("SERPENT.png")
    screen.blit(serpent, (x, y))




    # Affichage du score
    texte = "Score actuel : "
    texte_rendu = police.render(texte, True, blanc)
    texte_rect = texte_rendu.get_rect(center = (900, 50))
    screen.blit(texte_rendu, texte_rect)

    texte = str(score)
    texte_rendu = police.render(texte, True, blanc)
    texte_rect = texte_rendu.get_rect(center = (830, 90))
    screen.blit(texte_rendu, texte_rect)

    texte = "points"
    texte_rendu = police.render(texte, True, blanc)
    texte_rect = texte_rendu.get_rect(center = (900, 90))
    screen.blit(texte_rendu, texte_rect)



    # Dessine l'obstacle

    obstacle = pygame.image.load("CAILLOU.png")
    screen.blit(obstacle, (x_obstacle, y_obstacle))


    # Dessine la nourriture
    nourriture = pygame.image.load("litchi.png")
    screen.blit(nourriture, (x_nourriture, y_nourriture))
    if score >= 2 :
        nourriture2 = pygame.image.load("litchi.png")
        screen.blit(nourriture2, (x_nourriture2, y_nourriture2))
    if score >= 10 :
        nourriture3 = pygame.image.load("banane.png")
        screen.blit(nourriture3, (x_nourriture3, y_nourriture3))
    if score >= 20 :
        nourriture4 = pygame.image.load("litchi.png")
        screen.blit(nourriture4, (x_nourriture4, y_nourriture4))



    # Vérifie si le serpent a mangé la nourriture
    x_test1 = x_nourriture - 130
    x_test2 = x_nourriture - 10



    if x >= x_test1 and x <= x_test2 and y == y_nourriture  :
        x_nourriture = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
        y_nourriture = 0
        score = score + 1



    x_test1 = x_nourriture2 - 130
    x_test2 = x_nourriture2 - 10

    if x >= x_test1 and x <= x_test2 and y == y_nourriture2 :
        x_nourriture2 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
        y_nourriture2 = 0
        score = score + 1






    x_test1 = x_nourriture3 - 130
    x_test2 = x_nourriture3 - 10


    if x >= x_test1 and x <= x_test2 and y == y_nourriture3 :
        x_nourriture3 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
        y_nourriture3 = 0
        score = score + 2


    x_test1 = x_nourriture4 - 130
    x_test2 = x_nourriture4 - 10


    if x >= x_test1 and x <= x_test2 and y == y_nourriture4 :
        x_nourriture4 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
        y_nourriture4 = 0
        score = score + 1




    if y_nourriture >=810:
        x_nourriture = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
        y_nourriture = 0
    if y_nourriture2 >= 810 :
        x_nourriture2 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
        y_nourriture2 = 0
    if y_nourriture3 >= 810 :
        x_nourriture3 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
        y_nourriture3 = 0
    if y_nourriture4 >= 810 :
        x_nourriture4 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
        y_nourriture4 = 0

    # Vérifie si le serpent a tapé l'obstacle
    x_test3 = x_obstacle - 120
    x_test4 = x_obstacle - 30


    if x >= x_test3 and x <= x_test4 and y == y_obstacle :
        done = True
    if y_obstacle >= 810:
        x_obstacle = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
        y_obstacle = 0


    x_test3 = x_obstacle1 - 120
    x_test4 = x_obstacle1 - 30


    if score >= 2 :
        obstacle1 = pygame.image.load("CAILLOU.png")
        screen.blit(obstacle1, (x_obstacle1, y_obstacle1))
        if x >= x_test3 and x <= x_test4 and y == y_obstacle1 :
            done = True
        if y_obstacle1 >= 810:
            x_obstacle1 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
            y_obstacle1 = 0


    x_test3 = x_obstacle2 - 120
    x_test4 = x_obstacle2 - 30


    if score >= 5 :
        obstacle2 = pygame.image.load("CAILLOU.png")
        screen.blit(obstacle2, (x_obstacle2, y_obstacle2))
        if x >= x_test3 and x <= x_test4 and y == y_obstacle2 :
            done = True
        if y_obstacle2 >= 810:
            x_obstacle2 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
            y_obstacle2 = 0



    x_test3 = x_obstacle3 - 120
    x_test4 = x_obstacle3 - 30



    if score >= 10 :
        obstacle3 = pygame.image.load("CAILLOU.png")
        screen.blit(obstacle3, (x_obstacle3, y_obstacle3))
        if x >= x_test3 and x <= x_test4 and y == y_obstacle3 :
            done = True
        if y_obstacle3 >= 810:
            x_obstacle3 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
            y_obstacle3 = 0



    x_test3 = x_obstacle4 - 120
    x_test4 = x_obstacle4 - 30


    if score >= 15 :
        obstacle4 = pygame.image.load("CAILLOU.png")
        screen.blit(obstacle4, (x_obstacle4, y_obstacle4))
        if x >= x_test3 and x <= x_test4 and y == y_obstacle4 :
            done = True
        if y_obstacle4 >= 810:
            x_obstacle4 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
            y_obstacle4 = 0
            if deplacement == True:
                y_obstacle += 10
                y_obstacle1 += 10
                y_obstacle2 += 10
                y_obstacle3 += 10
                y_obstacle4 += 10


    x_test3 = x_obstacle5 - 120
    x_test4 = x_obstacle5 - 30


    if score >= 30 :
        obstacle5 = pygame.image.load("CAILLOU.png")
        screen.blit(obstacle5, (x_obstacle5, y_obstacle5))
        if x >= x_test3 and x <= x_test4 and y == y_obstacle5 :
            done = True
        if y_obstacle5 >= 810:
            x_obstacle5 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
            y_obstacle5 = 0
            if deplacement == True:
                y_obstacle += 15
                y_obstacle1 += 15
                y_obstacle2 += 15
                y_obstacle3 += 15
                y_obstacle4 += 15
                y_obstacle5 += 15

    x_test3 = x_obstacle6 - 120
    x_test4 = x_obstacle6 - 30


    if score >= 40 :
        obstacle6 = pygame.image.load("CAILLOU.png")
        screen.blit(obstacle6, (x_obstacle6, y_obstacle6))
        if x >= x_test3 and x <= x_test4 and y == y_obstacle6 :
            done = True
        if y_obstacle6 >= 810:
            x_obstacle6 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
            y_obstacle6 = 0
            if deplacement == True:
                y_obstacle += 20
                y_obstacle1 += 20
                y_obstacle2 += 20
                y_obstacle3 += 20
                y_obstacle4 += 20
                y_obstacle5 += 20
                y_obstacle6 += 20


    x_test3 = x_obstacle7 - 120
    x_test4 = x_obstacle7 - 30


    if score >= 50 :
        obstacle7 = pygame.image.load("CAILLOU.png")
        screen.blit(obstacle7, (x_obstacle7, y_obstacle7))
        if x >= x_test3 and x <= x_test4 and y == y_obstacle7 :
            done = True
        if y_obstacle7 >= 810:
            x_obstacle7 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
            y_obstacle7 = 0
            if deplacement == True:
                y_obstacle += 25
                y_obstacle1 += 25
                y_obstacle2 += 25
                y_obstacle3 += 25
                y_obstacle4 += 25
                y_obstacle5 += 25
                y_obstacle6 += 25
                y_obstacle7 += 25


    x_test3 = x_obstacle8 - 120
    x_test4 = x_obstacle8 - 30


    if score >= 60 :
        obstacle8 = pygame.image.load("CAILLOU.png")
        screen.blit(obstacle8, (x_obstacle8, y_obstacle8))
        if x >= x_test3 and x <= x_test4 and y == y_obstacle8 :
            done = True
        if y_obstacle8 >= 810:
            x_obstacle8 = random.randrange(0, largeur_ecran - taille_serpent, taille_serpent)
            y_obstacle8 = 0
            if deplacement == True:
                y_obstacle += 30
                y_obstacle1 += 30
                y_obstacle2 += 30
                y_obstacle3 += 30
                y_obstacle4 += 30
                y_obstacle5 += 30
                y_obstacle6 += 30
                y_obstacle7 += 30
                y_obstacle8 += 30


    # Limite de rafraîchissement
        clock.tick(60)



    pygame.display.update()

# Fermeture de Pygame
go = pygame.image.load("gameover.png")
screen.blit(go,(0,0))
pygame.display.update()
testfin = True
while testfin == True:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            testfin = False

pygame.quit()
pygame.quit()







