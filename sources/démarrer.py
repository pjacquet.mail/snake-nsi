# Créé par BERTHOY, le 26/03/2024 en Python 3.7
import pygame
import subprocess

# Initialisation de Pygame
pygame.init()

# Création de la fenêtre de jeu
fenetre = pygame.display.set_mode((300, 300))

# Chargement des image
jouer1 = pygame.image.load('jouer classique.png').convert_alpha()
jouer2 = pygame.image.load('jouer rapide.png').convert_alpha()
fond = pygame.image.load("fond.png")

# Création des rectangles pour les boutons
rect1 = jouer1.get_rect()
rect2 = jouer2.get_rect()
rect1.topleft = (120, 125) # Positionnement du rectangle
rect2.topleft = (120,175)
#mise en place du fond d'écran
fenetre.blit(fond,(0,0))

# Boucle principale du menu
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            # Vérifie si le clic est sur l'image
            if rect1.collidepoint(event.pos):
                subprocess.run(['python','snake ouvert.py'])

            if rect2.collidepoint(event.pos):
                subprocess.run(['python','snake rapide.py'])



    # Placement de l'image sur le rectangle
    fenetre.blit(jouer1, rect1)
    fenetre.blit(jouer2, rect2)

    # Mise à jour de l'affichage
    pygame.display.flip()

# Quitter Pygame
pygame.quit()
