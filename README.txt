Protocole d’utilisation :
Il faut tout d’abord ouvrir le programme « démarrer » et lancer le programme. 
Ensuite, une petite fenêtre apparait, vous devez sélectionnez le mode de jeu souhaité. 
Mode classique ou mode rapide. 
Le mode classique consiste à récolter des fruits tout en évitant les obstacles. 
Il y a plusieurs types de fruits et plus votre score augmente plus les obstacles et les fruits augmentent. 
Le mode rapide est une version avec moins de fruits et un peu moins d’obstacle mais tout va plus vite. 
A vous de choisir quel mode vous préférez. Une fois que la partie est terminée, 
une fenêtre de fin s’affiche vous n’avez plus qu’à la fermer et à relancer le programme pour continuer 
de vous amuser.
